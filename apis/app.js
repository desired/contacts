import express from "express";

import cors from 'cors'
import './src/config'
import Contact from './src/routes/contact'

require('dotenv').config({path: `./development.env`})

const app = express()

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors())
app.use('/contacts', Contact)
app.listen(process.env.PORT)