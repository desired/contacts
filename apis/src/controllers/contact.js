import * as getContactService from '../services/contact/get'
import * as putContactService from '../services/contact/put'
import * as postContactService from '../services/contact/post'
import * as deleteContactService from '../services/contact/delete'

export const fetchContacts = async (req, res) => {
    try{
        res.status(200).json((await getContactService.all()))
    } catch (e) {
        res.status(500).json(e)
    }
}

export const fetchContact = async (req, res) => {
    try{
        res.status(200).json((await getContactService.byId(req.params.id)))
    } catch (e) {
        res.status(500).json(e)
    }
}

export const createContacts = async (req, res) => {
    try{
        const {
            nom,
            prenoms,
            email,
            telPortable,
            telDomicile
        } = req.body

        res.status(201).json((await postContactService.create({
            nom,
            prenoms,
            email,
            telPortable,
            telDomicile
        })))
    } catch (e) {
        console.log('error: ----- ', e)
        res.status(500).json(e)
    }
}

export const updateContacts = async (req, res) => {
    try{
        const {
            nom,
            prenoms,
            email,
            telPortable,
            telDomicile
        } = req.body
        res.status(200).json((await putContactService.byId(req.params.id, {
            nom,
            prenoms,
            email,
            telPortable,
            telDomicile
        })))
    } catch (e) {
        res.status(500).json(e)
    }
}

export const deleteContact = async (req, res) => {
    try{
        console.log(' deleteContact 111111111 ')
        res.status(200).json((await deleteContactService.deleteById(req.params.id)))
    } catch (e) {
        console.log(' error : ', e)
        res.status(500).json(e)
    }
}