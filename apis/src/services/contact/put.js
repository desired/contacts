import { Contact } from "../../models/contact"

export const byId = async (id, contact) => Contact.findByIdAndUpdate(id, contact)