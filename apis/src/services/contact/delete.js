import { Contact } from "../../models/contact"

export const deleteById = async (id) => Contact.findByIdAndDelete(id)