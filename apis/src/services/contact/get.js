import { Contact } from "../../models/contact"

export const all = async () => Contact.find({})

export const byId = async (id) => Contact.findById(id)