import { Contact } from "../../models/contact"

export const create = async (contact) => Contact.create(contact)