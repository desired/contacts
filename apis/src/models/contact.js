import mongoose from "mongoose";

export const Contact = mongoose.model('contact', new mongoose.Schema({
    nom: String,
    prenoms: String,
    email: String,
    telPortable: String,
    telDomicile: String
}))
