import { Router } from "express";
import * as contactController from '../controllers/contact';
const Contact = Router();

Contact.get('/', contactController.fetchContacts)

Contact.get('/:id', contactController.fetchContact)

Contact.post('/', contactController.createContacts)

Contact.put('/:id', contactController.updateContacts)

Contact.delete('/:id', contactController.deleteContact)

export default Contact