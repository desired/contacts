export const state = () => ({
    id: "",
    nom: "",
    prenoms: "",
    email: "",
    telPortable: "",
    telDomicile: ""
})

export const mutations = {
    setId: (state, data) => {
        state.id = data
    },
    setNom: (state, data) => {
        state.nom = data
    },
    setPrenoms: (state, data) => {
        state.prenoms = data
    },
    setEmail: (state, data) => {
        state.email = data
    },
    setTelPortable: (state, data) => {
        state.telPortable = data
    },
    setTelDomicile: (state, data) => {
        state.telDomicile = data
    }
}